# Vue 3 + Vite

This template should help get you started developing with Vue 3 in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)


## Install and run locally

This project uses Vue 3 and Vite. 

To install dependencies:
```
npm install
```
To run the project (Default port is 3000): 
```
npm run dev
```

## Authors
Arvin Khodabandeh
Håkon Holm Erstad
