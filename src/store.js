import { createStore, storeKey } from "vuex";
import { apiFetchQuestions } from "./api/questions";
import { apiCreateUser, apiFindUsername, apiUpdateUser } from "./api/users";



export default createStore({
    state: {
        user: {
            id: 0,
            username: "",
            highScore: 0
        },
        questions: [],
        error: "",
        currentQuestion: {},
        currentAnswers: [],
        currentSelectedAnswer: "",
        selectedAnswers: [],
        correctAnswers: [],
        questionParams: {
            amount: 10,
            category: 0,
            difficulty: "",
            type: "",
            token: ""
        },
        isLastQuestionAnswered: false,
        totalPoints: 0
    },
    mutations: {
        setUser: (state, user) => {
            state.user.id = user.id
            state.user.username = user.username
            state.user.highScore = user.highScore 
        },
        setHighScore: (state, highscore) => {
            state.user.highScore = highscore.value
        },
        setQuestions: (state, questions) => {
            state.questions = questions;
        },
        setError: (state, error) => {
            state.error = error;
        },
        setCurrentQuestion: (state, currentQuestionNumber) => {
            state.currentQuestion = state.questions[currentQuestionNumber];
        },
        setCurrentAnswers: (state, currentAnswers) => {
            state.currentAnswers = currentAnswers;
        },
        setSelectedAnswers: (state, selectedAnswer) => {
            state.selectedAnswers = selectedAnswer
        },
        setCorrectAnswers: (state, correctAnswers) => {
            state.correctAnswers = correctAnswers
        },
        appendSelectedAnswer: (state, selectedAnswer) => {
            const currentSelectedAnswer = {
                id: state.currentQuestion.id,
                selectedAnswer
            }
            state.selectedAnswers.push(currentSelectedAnswer);
        },
        appendCorrectAnswer: (state, correct_answer) => {
            const currentCorrectAnswer = {
                id: state.currentQuestion.id,
                correct_answer
            }
            state.correctAnswers.push(currentCorrectAnswer);
        },
        setTotalPoints: (state) => {
            state.totalPoints = 0
        },
        increasePoints: (state) => {
            state.totalPoints += 10;
        },
        setQuestionParams: (state, questionParams) => {
            state.questionParams.amount = questionParams.amount
            state.questionParams.category = questionParams.category
            state.questionParams.difficulty = questionParams.difficulty
            state.questionParams.type = questionParams.type
            state.questionParams.token = questionParams.token
        },
        lastQuestionAnswered: (state, value) => {
            state.isLastQuestionAnswered = value;
        }

    },
    getters: {
        numOfQuestions: (state) => {
            return state.questions.length;
        },
        currentQuestionNumber: (state) => {
            return state.currentQuestion.id + 1;
        },
        getError: (state) => {
            return state.error
        },
        getUsername: (state) => {
            return state.user.username
        },
        getTotalPoints: (state) => {
            return state.totalPoints
        },
        getCurrentCategory: (state) => {
            return state.questionParams.category
        }
    },
    actions: {
        fetchAllQuestions: async ({ commit, state }) => {
            const [err, questions] = await apiFetchQuestions(state.questionParams);
            if (err) {
                commit("setError", err);
            } else {
                commit("setQuestions", questions);
                commit("setCurrentQuestion", 0);
            }
            //return Promise.resolve();
        },
        getCurrentAnswers: ({ commit }, currentQuestion) => {
            const answers = [currentQuestion.correct_answer]
            for (let i = 0; i < currentQuestion.incorrect_answers.length; i++) {
                answers.push(currentQuestion.incorrect_answers[i]);
            }

            answers.sort(function(a, b) {
                return 0.5 - Math.random()
            });
            commit("appendCorrectAnswer", currentQuestion.correct_answer);
            commit("setCurrentAnswers", answers);
        },
        selectAnswer: ({ commit, getters },selectedAnswer) => {
            const currentQuestionNumber = getters.currentQuestionNumber;
            const numOfQuestions = getters.numOfQuestions;
            commit("appendSelectedAnswer", selectedAnswer);
            if (currentQuestionNumber >= numOfQuestions) {
                commit("lastQuestionAnswered", true);
                // Route to results
            }
        },
        getNextQuestion: ({ commit, getters }) => {
            const currentQuestionNumber = getters.currentQuestionNumber;
            const numOfQuestions = getters.numOfQuestions;
            if(currentQuestionNumber < numOfQuestions) {
                commit("setCurrentQuestion", currentQuestionNumber);
            } 
            
        },
        increasePoints: ({ commit }) => {
            commit("increasePoints");
        },
        fetchExistingUser: async ({commit}, username) => {
            const [ error, user ] = await apiFindUsername(username)
            if(error !== null) {
                return error
            }

            commit("setUser", user[0])
            return null

        },
        addNewUser: async ({commit}, username) => {
            const [ error, user ] = await apiCreateUser(username)
            if(error !== null) {
                return error
            }

            commit("setUser", user)
            return null
        },
        updateUserDatabase: async ({commit, state}, highscore) => {
            const [ error, user ] = await apiUpdateUser(highscore, state.user.id)
            if(error !== null) {
                return error
            }

            commit("setUser", user)
            return null
        },
        fetchQuestionParams: async ({commit}, questionParams) => {
            commit("setQuestionParams", questionParams)
            return null
        },
        resetError: async ({commit}, emptyString) => {
            commit("setError", emptyString)
            return null
        },
        replayGame: async ({commit}) => {
            const questions = []
            const currentAnswers = []
            const selectedAnswers = []
            const correctAnswers = []
            const isLastQuestionAnswered = false
            
            commit("setQuestions", questions)
            commit("setCurrentAnswers", currentAnswers)
            commit("setSelectedAnswers", selectedAnswers)
            commit("setCorrectAnswers", correctAnswers)
            commit("setTotalPoints")
            commit("lastQuestionAnswered", isLastQuestionAnswered)
        },
        fullGameReset: async ({commit}) => {
            const user = {
                id: 0,
                username: "",
                highScore: 0
            }
            const questions = []
            const currentAnswers = []
            const selectedAnswers = []
            const correctAnswers = []
            const questionParams = {
                amount: 10,
                category: 0,
                difficulty: "",
                type: "",
                token: ""
            }
            const isLastQuestionAnswered = false

            commit("setUser", user)
            commit("setQuestions", questions)
            commit("setCurrentAnswers", currentAnswers)
            commit("setQuestionParams", questionParams)
            commit("setSelectedAnswers", selectedAnswers)
            commit("setCorrectAnswers", correctAnswers)
            commit("setTotalPoints")
            commit("lastQuestionAnswered", isLastQuestionAnswered)
        }
    }
})