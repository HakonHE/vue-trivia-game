import { createRouter, createWebHistory } from "vue-router";
import store from "./store";
import Start from "./views/Start.vue";
import Question from "./views/Question.vue";
import Result from "./views/Result.vue";


const authGuard = (to, from, next) => {
    if (!store.state.user.username) {
        next("/")
    } else {
        next()
    }
}

const loginGuard = (_to, _from, next) => {
    if(store.state.user.username) {
        next("/question")
    } else {
        next()
    }
}



const routes = [
    {
        path: "/",
        component: Start,
        beforeEnter: loginGuard
    },
    {
        path: "/question",
        component: Question,
        beforeEnter: authGuard
    },
    {
        path: "/result",
        component: Result,
        beforeEnter: authGuard
    }
]

export default createRouter({
    history: createWebHistory(),
    routes
})