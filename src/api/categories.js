/**
 * Fetches categories available from api
 * @returns categories
 */
export async function apiFetchCategories () {
    const CATEGORY_URL = "https://opentdb.com/api_category.php"
    
    try {
        const response = await fetch(CATEGORY_URL)
        if (!response.ok) {
            if (response.status === 404) {
                throw new Error("Invalid URL.")
            }
            throw new Error("Could not fetch categories.")
        }
        const json = await response.json()
        return [ null, json.trivia_categories ]
    }
    catch(e) {
        return [e.message, []]
    }
}

/**
 * Fetches number of questions for a category
 * @param {number} categoryId 
 * @returns {Array}
 */
export async function apiFetchCategoryQuestionNumber (categoryId) {
    const CATEGORY_NUMBER_URL = `https://opentdb.com/api_count.php?category=${categoryId}`
    
    try {
        const response = await fetch(CATEGORY_NUMBER_URL)
        if (!response.ok) {
            if (response.status === 404) {
                throw new Error("Invalid URL.")
            }
            throw new Error("Could not fetch categories.")
        }
        const json = await response.json()
        
        return [ null, json.category_question_count ] 
    }
    catch(e) {
        return [e.message, []]
    }
}