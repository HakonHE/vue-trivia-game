const apiURL = 'https://blueteam-noroff-api.herokuapp.com'
const apiKey = '9bds9K5fbgJQRFkefaSogG7lnH4k7lw1IsKIZQXKg9cmZHLJWR7DLZE5UnBkHb0D'


/**
 * Finds if username in database
 * @param {string} username 
 * @returns JSON object containing the user's data
 */
export async function apiFindUsername(username) {
    try {
        const response = await fetch(`${apiURL}/trivia?username=${username}`)
        if (!response.ok) {
            if (response.status === 404) {
                throw new Error("Invalid URL.")
            }
            throw new Error("Could not fetch user.")
        }
        const json = await response.json()
        return [ null, json ] // the users
    }
    catch(e) {
        return [e.message, []]
    }
}

/**
 * Creates a user in the api database
 * @param {string} username 
 * @returns JSON object containing userdata
 */
export async function apiCreateUser(username) {
    try {
        const response = await fetch(`${apiURL}/trivia`, {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ 
                username: username, 
                highScore: 0 
            })
        })
        if (!response.ok) {
            throw new Error('Could not create new user')
        }
        const json = await response.json()
        return [null, json]
    }
    catch(e) {
        return [e.message, []]
    }
}

/**
 * Updates an already existing user with new score
 * @param {int} highscore 
 * @param {int} userId 
 * @returns JSON Object with user data
 */
export async function apiUpdateUser(highscore, userId) {
    try {
        const response = await fetch(`${apiURL}/trivia/${userId}`, {
            method: 'PATCH', // NB: Set method to PATCH
            headers: {
                'X-API-Key': apiKey,
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                // Provide new highScore to add to user with id 1
                highScore: highscore  
            })
        })
        if (!response.ok) {
            throw new Error('Could not update high score')
        }
        const json = await response.json()
        return [null, json]

    }
    catch (e) {
        return [e.message, []]
    }    
}