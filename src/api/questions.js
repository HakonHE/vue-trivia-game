/**
 * Parses information and builds a url string and fetches the 
 * corresponding questions JSON object from the API based
 * on the desired amount of questions, category, difficulty,
 * and type of question, as well as a session token.
 * @param {Object} questionParams  
 * @returns JSON Object containing questions
 */

export async function apiFetchQuestions (questionParams) {

    let url = new URL("https://opentdb.com/api.php?");
    if (questionParams.amount > 0) {
        url.searchParams.append("amount", questionParams.amount);
    }
    if (questionParams.category > 0) {
        url.searchParams.append("category", questionParams.category);
    }
    if (questionParams.difficulty !== (undefined || "")) {
        url.searchParams.append("difficulty", questionParams.difficulty);
    }
    if (questionParams.type !== (undefined || "")) {
        url.searchParams.append("type", questionParams.type);
    }
    if (questionParams.token !== (undefined || "")) {
        url.searchParams.append("token", questionParams.token);
    }

    try {
        const response = await fetch(url.toString())
        if (!response.ok) {
            if (response.status === 404) {
                throw new Error("Invalid URL.")
            }
            throw new Error("Could not fetch categories.")
        }
        const json = await response.json()
        if (json.response_code > 0) {
            throw new Error("Not enough available questions with the given parameters!");
        }
        let questions = json.results;

        for (let i = 0; i < questions.length; i++) {
            questions[i].id = i;
        }
        return [ null, questions ] 
    }
    catch(e) {
        return [e.message, []]
    }
}

/**
 * Request a new session token from the API.
 * @returns String containing a session token
 */
export async function apiFetchSessionToken () {
    const REQUEST_TOKEN_URL = `https://opentdb.com/api_token.php?command=request`
    
    try {
        const response = await fetch(REQUEST_TOKEN_URL)
        if (!response.ok) {
            if (response.status === 404) {
                throw new Error("Invalid URL.")
            }
            throw new Error("Could not fetch token.")
        }
        const json = await response.json()
        
        return [ null, json.token ] 
    }
    catch(e) {
        return [e.message, []]
    }
}