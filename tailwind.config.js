module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue, js}"
  ],
  theme: {
    extend: {
      colors: {
        blue: {
          munsell: '#068D9D',
          darkslate: '#414288'
        },
        green: {
          celadon: '#98DFAF'
        },
        red: {
          flame: '#DD6031'
        },
        purple: {
          palpatine: '#682D63'
        }
      }
    },
  },
  plugins: [],
}
